﻿using SPM_BI.DataAccess;
using System;

namespace SPM_BI.UI
{
    public class Program
    {
        private static DataContext _context = new DataContext();
        static void Main(string[] args)
        {
            _context.Database.EnsureCreated();
        }
    }
}
