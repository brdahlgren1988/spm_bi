﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SPM_BI.DataAccess.Migrations
{
    public partial class UpdateSaveDefaultValues : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DateUpdated",
                table: "SalesTerritory",
                newName: "UpdatedDate");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "SalesTerritory",
                newName: "CreatedDate");

            migrationBuilder.RenameColumn(
                name: "DateUpdated",
                table: "SalesSegment",
                newName: "UpdatedDate");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "SalesSegment",
                newName: "CreatedDate");

            migrationBuilder.RenameColumn(
                name: "DateUpdated",
                table: "InvoiceLineItem",
                newName: "UpdatedDate");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "InvoiceLineItem",
                newName: "CreatedDate");

            migrationBuilder.RenameColumn(
                name: "DateUpdated",
                table: "InvoiceHeader",
                newName: "UpdatedDate");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "InvoiceHeader",
                newName: "CreatedDate");

            migrationBuilder.RenameColumn(
                name: "DateUpdated",
                table: "Employee",
                newName: "UpdatedDate");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "Employee",
                newName: "CreatedDate");

            migrationBuilder.RenameColumn(
                name: "DateUpdated",
                table: "Customer",
                newName: "UpdatedDate");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "Customer",
                newName: "CreatedDate");

            migrationBuilder.RenameColumn(
                name: "DateUpdated",
                table: "ChargeCode",
                newName: "UpdatedDate");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "ChargeCode",
                newName: "CreatedDate");

            migrationBuilder.RenameColumn(
                name: "DateUpdated",
                table: "Assignment",
                newName: "UpdatedDate");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "Assignment",
                newName: "CreatedDate");

            migrationBuilder.RenameColumn(
                name: "DateUpdated",
                table: "Alignment",
                newName: "UpdatedDate");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "Alignment",
                newName: "CreatedDate");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "UpdatedDate",
                table: "SalesTerritory",
                newName: "DateUpdated");

            migrationBuilder.RenameColumn(
                name: "CreatedDate",
                table: "SalesTerritory",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "UpdatedDate",
                table: "SalesSegment",
                newName: "DateUpdated");

            migrationBuilder.RenameColumn(
                name: "CreatedDate",
                table: "SalesSegment",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "UpdatedDate",
                table: "InvoiceLineItem",
                newName: "DateUpdated");

            migrationBuilder.RenameColumn(
                name: "CreatedDate",
                table: "InvoiceLineItem",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "UpdatedDate",
                table: "InvoiceHeader",
                newName: "DateUpdated");

            migrationBuilder.RenameColumn(
                name: "CreatedDate",
                table: "InvoiceHeader",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "UpdatedDate",
                table: "Employee",
                newName: "DateUpdated");

            migrationBuilder.RenameColumn(
                name: "CreatedDate",
                table: "Employee",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "UpdatedDate",
                table: "Customer",
                newName: "DateUpdated");

            migrationBuilder.RenameColumn(
                name: "CreatedDate",
                table: "Customer",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "UpdatedDate",
                table: "ChargeCode",
                newName: "DateUpdated");

            migrationBuilder.RenameColumn(
                name: "CreatedDate",
                table: "ChargeCode",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "UpdatedDate",
                table: "Assignment",
                newName: "DateUpdated");

            migrationBuilder.RenameColumn(
                name: "CreatedDate",
                table: "Assignment",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "UpdatedDate",
                table: "Alignment",
                newName: "DateUpdated");

            migrationBuilder.RenameColumn(
                name: "CreatedDate",
                table: "Alignment",
                newName: "DateCreated");
        }
    }
}
