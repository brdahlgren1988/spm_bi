﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SPM_BI.DataAccess.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Customer",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Employee",
                columns: table => new
                {
                    EmployeeID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee", x => x.EmployeeID);
                });

            migrationBuilder.CreateTable(
                name: "SalesSegment",
                columns: table => new
                {
                    SalesSegmentID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SalesSegment", x => x.SalesSegmentID);
                });

            migrationBuilder.CreateTable(
                name: "SalesTerritory",
                columns: table => new
                {
                    SalesTerritoryID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Division = table.Column<int>(type: "int", nullable: false),
                    Group = table.Column<int>(type: "int", nullable: false),
                    Organization = table.Column<int>(type: "int", nullable: false),
                    Region = table.Column<int>(type: "int", nullable: false),
                    Area = table.Column<int>(type: "int", nullable: false),
                    Distric = table.Column<int>(type: "int", nullable: false),
                    Territory = table.Column<int>(type: "int", nullable: false),
                    SalesSegmentID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SalesTerritory", x => x.SalesTerritoryID);
                    table.ForeignKey(
                        name: "FK_SalesTerritory_SalesSegment_SalesSegmentID",
                        column: x => x.SalesSegmentID,
                        principalTable: "SalesSegment",
                        principalColumn: "SalesSegmentID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Alignment",
                columns: table => new
                {
                    AlignmentID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CustomerID = table.Column<int>(type: "int", nullable: true),
                    SalesTerritoryID = table.Column<int>(type: "int", nullable: true),
                    EffectiveDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ExpirationDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Alignment", x => x.AlignmentID);
                    table.ForeignKey(
                        name: "FK_Alignment_Customer_CustomerID",
                        column: x => x.CustomerID,
                        principalTable: "Customer",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Alignment_SalesTerritory_SalesTerritoryID",
                        column: x => x.SalesTerritoryID,
                        principalTable: "SalesTerritory",
                        principalColumn: "SalesTerritoryID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Assignment",
                columns: table => new
                {
                    AssignmentID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SalesTerritoryID = table.Column<int>(type: "int", nullable: true),
                    EmployeeID = table.Column<int>(type: "int", nullable: true),
                    EffectiveDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ExpirationDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Assignment", x => x.AssignmentID);
                    table.ForeignKey(
                        name: "FK_Assignment_Employee_EmployeeID",
                        column: x => x.EmployeeID,
                        principalTable: "Employee",
                        principalColumn: "EmployeeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Assignment_SalesTerritory_SalesTerritoryID",
                        column: x => x.SalesTerritoryID,
                        principalTable: "SalesTerritory",
                        principalColumn: "SalesTerritoryID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Alignment_CustomerID",
                table: "Alignment",
                column: "CustomerID");

            migrationBuilder.CreateIndex(
                name: "IX_Alignment_SalesTerritoryID",
                table: "Alignment",
                column: "SalesTerritoryID");

            migrationBuilder.CreateIndex(
                name: "IX_Assignment_EmployeeID",
                table: "Assignment",
                column: "EmployeeID");

            migrationBuilder.CreateIndex(
                name: "IX_Assignment_SalesTerritoryID",
                table: "Assignment",
                column: "SalesTerritoryID");

            migrationBuilder.CreateIndex(
                name: "IX_SalesTerritory_SalesSegmentID",
                table: "SalesTerritory",
                column: "SalesSegmentID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Alignment");

            migrationBuilder.DropTable(
                name: "Assignment");

            migrationBuilder.DropTable(
                name: "Customer");

            migrationBuilder.DropTable(
                name: "Employee");

            migrationBuilder.DropTable(
                name: "SalesTerritory");

            migrationBuilder.DropTable(
                name: "SalesSegment");
        }
    }
}
