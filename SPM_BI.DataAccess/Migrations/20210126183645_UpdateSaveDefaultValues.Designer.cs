﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using SPM_BI.DataAccess;

namespace SPM_BI.DataAccess.Migrations
{
    [DbContext(typeof(DataContext))]
    [Migration("20210126183645_UpdateSaveDefaultValues")]
    partial class UpdateSaveDefaultValues
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .UseIdentityColumns()
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.2");

            modelBuilder.Entity("SPM_BI.Model.Alignment", b =>
                {
                    b.Property<int>("AlignmentID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("CreatedBy")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("CreatedDate")
                        .HasColumnType("datetime2");

                    b.Property<int?>("CustomerID")
                        .HasColumnType("int");

                    b.Property<DateTime>("EffectiveDate")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("ExpirationDate")
                        .HasColumnType("datetime2");

                    b.Property<int?>("SalesTerritoryID")
                        .HasColumnType("int");

                    b.Property<string>("UpdatedBy")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("UpdatedDate")
                        .HasColumnType("datetime2");

                    b.HasKey("AlignmentID");

                    b.HasIndex("CustomerID");

                    b.HasIndex("SalesTerritoryID");

                    b.ToTable("Alignment");
                });

            modelBuilder.Entity("SPM_BI.Model.Assignment", b =>
                {
                    b.Property<int>("AssignmentID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("CreatedBy")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("CreatedDate")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("EffectiveDate")
                        .HasColumnType("datetime2");

                    b.Property<int?>("EmployeeID")
                        .HasColumnType("int");

                    b.Property<DateTime>("ExpirationDate")
                        .HasColumnType("datetime2");

                    b.Property<int?>("SalesTerritoryID")
                        .HasColumnType("int");

                    b.Property<string>("UpdatedBy")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("UpdatedDate")
                        .HasColumnType("datetime2");

                    b.HasKey("AssignmentID");

                    b.HasIndex("EmployeeID");

                    b.HasIndex("SalesTerritoryID");

                    b.ToTable("Assignment");
                });

            modelBuilder.Entity("SPM_BI.Model.ChargeCode", b =>
                {
                    b.Property<int>("ChargeCodeID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("CreatedBy")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("CreatedDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UpdatedBy")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("UpdatedDate")
                        .HasColumnType("datetime2");

                    b.HasKey("ChargeCodeID");

                    b.ToTable("ChargeCode");
                });

            modelBuilder.Entity("SPM_BI.Model.Customer", b =>
                {
                    b.Property<int>("CustomerID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("CreatedBy")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("CreatedDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("UpdatedBy")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("UpdatedDate")
                        .HasColumnType("datetime2");

                    b.HasKey("CustomerID");

                    b.ToTable("Customer");
                });

            modelBuilder.Entity("SPM_BI.Model.Employee", b =>
                {
                    b.Property<int>("EmployeeID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("CreatedBy")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("CreatedDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("FirstName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("LastName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UpdatedBy")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("UpdatedDate")
                        .HasColumnType("datetime2");

                    b.HasKey("EmployeeID");

                    b.ToTable("Employee");
                });

            modelBuilder.Entity("SPM_BI.Model.InvoiceHeader", b =>
                {
                    b.Property<int>("InvoiceHeaderID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("CreatedBy")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("CreatedDate")
                        .HasColumnType("datetime2");

                    b.Property<int>("CustomerID")
                        .HasColumnType("int");

                    b.Property<string>("UpdatedBy")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("UpdatedDate")
                        .HasColumnType("datetime2");

                    b.HasKey("InvoiceHeaderID");

                    b.HasIndex("CustomerID");

                    b.ToTable("InvoiceHeader");
                });

            modelBuilder.Entity("SPM_BI.Model.InvoiceLineItem", b =>
                {
                    b.Property<int>("InvoiceLineItemID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<int>("ChargeCodeID")
                        .HasColumnType("int");

                    b.Property<string>("CreatedBy")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("CreatedDate")
                        .HasColumnType("datetime2");

                    b.Property<int>("InvoiceHeaderID")
                        .HasColumnType("int");

                    b.Property<float>("Revenue")
                        .HasColumnType("real");

                    b.Property<string>("UpdatedBy")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("UpdatedDate")
                        .HasColumnType("datetime2");

                    b.HasKey("InvoiceLineItemID");

                    b.HasIndex("ChargeCodeID");

                    b.HasIndex("InvoiceHeaderID");

                    b.ToTable("InvoiceLineItem");
                });

            modelBuilder.Entity("SPM_BI.Model.SalesSegment", b =>
                {
                    b.Property<int>("SalesSegmentID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("CreatedBy")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("CreatedDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UpdatedBy")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("UpdatedDate")
                        .HasColumnType("datetime2");

                    b.HasKey("SalesSegmentID");

                    b.ToTable("SalesSegment");
                });

            modelBuilder.Entity("SPM_BI.Model.SalesTerritory", b =>
                {
                    b.Property<int>("SalesTerritoryID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<int>("Area")
                        .HasColumnType("int");

                    b.Property<string>("CreatedBy")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("CreatedDate")
                        .HasColumnType("datetime2");

                    b.Property<int>("Distric")
                        .HasColumnType("int");

                    b.Property<int>("Division")
                        .HasColumnType("int");

                    b.Property<int>("Group")
                        .HasColumnType("int");

                    b.Property<int>("Organization")
                        .HasColumnType("int");

                    b.Property<int>("Region")
                        .HasColumnType("int");

                    b.Property<int>("SalesSegmentID")
                        .HasColumnType("int");

                    b.Property<int>("Territory")
                        .HasColumnType("int");

                    b.Property<string>("UpdatedBy")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("UpdatedDate")
                        .HasColumnType("datetime2");

                    b.HasKey("SalesTerritoryID");

                    b.HasIndex("SalesSegmentID");

                    b.ToTable("SalesTerritory");
                });

            modelBuilder.Entity("SPM_BI.Model.Alignment", b =>
                {
                    b.HasOne("SPM_BI.Model.Customer", "Customer")
                        .WithMany()
                        .HasForeignKey("CustomerID");

                    b.HasOne("SPM_BI.Model.SalesTerritory", "SalesTerritory")
                        .WithMany()
                        .HasForeignKey("SalesTerritoryID");

                    b.Navigation("Customer");

                    b.Navigation("SalesTerritory");
                });

            modelBuilder.Entity("SPM_BI.Model.Assignment", b =>
                {
                    b.HasOne("SPM_BI.Model.Employee", "Employee")
                        .WithMany()
                        .HasForeignKey("EmployeeID");

                    b.HasOne("SPM_BI.Model.SalesTerritory", "SalesTerritory")
                        .WithMany()
                        .HasForeignKey("SalesTerritoryID");

                    b.Navigation("Employee");

                    b.Navigation("SalesTerritory");
                });

            modelBuilder.Entity("SPM_BI.Model.InvoiceHeader", b =>
                {
                    b.HasOne("SPM_BI.Model.Customer", "Customer")
                        .WithMany()
                        .HasForeignKey("CustomerID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Customer");
                });

            modelBuilder.Entity("SPM_BI.Model.InvoiceLineItem", b =>
                {
                    b.HasOne("SPM_BI.Model.ChargeCode", "ChargeCode")
                        .WithMany()
                        .HasForeignKey("ChargeCodeID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("SPM_BI.Model.InvoiceHeader", "InvoiceHeader")
                        .WithMany("InvoiceLineItems")
                        .HasForeignKey("InvoiceHeaderID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("ChargeCode");

                    b.Navigation("InvoiceHeader");
                });

            modelBuilder.Entity("SPM_BI.Model.SalesTerritory", b =>
                {
                    b.HasOne("SPM_BI.Model.SalesSegment", "SalesSegment")
                        .WithMany()
                        .HasForeignKey("SalesSegmentID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("SalesSegment");
                });

            modelBuilder.Entity("SPM_BI.Model.InvoiceHeader", b =>
                {
                    b.Navigation("InvoiceLineItems");
                });
#pragma warning restore 612, 618
        }
    }
}
