﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SPM_BI.DataAccess.Migrations
{
    public partial class CustomerHierarchy : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CustomerHierarchy",
                columns: table => new
                {
                    CustomerHierarchyID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GlobalID = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CountryID = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FacilityID = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CustomerID = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerHierarchy", x => x.CustomerHierarchyID);
                    table.ForeignKey(
                        name: "FK_CustomerHierarchy_Customer_CustomerID",
                        column: x => x.CustomerID,
                        principalTable: "Customer",
                        principalColumn: "CustomerID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomerHierarchy_CustomerID",
                table: "CustomerHierarchy",
                column: "CustomerID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomerHierarchy");
        }
    }
}
