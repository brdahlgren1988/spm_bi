﻿using Microsoft.EntityFrameworkCore;
using SPM_BI.Model.Base;
using SPM_BI.Model.Interfaces;
using System;
using System.Linq;

namespace SPM_BI.DataAccess
{
    public class DataContext : DbContext
    {
        public string UserId { get; set; }

        public DbSet<Alignment> Alignment { get; set; }
        public DbSet<Assignment> Assignment { get; set; }
        public DbSet<ChargeCode> ChargeCode { get; set; }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<CustomerHierarchy> CustomerHierarchy { get; set; }
        public DbSet<Employee> Employee { get; set; }
        public DbSet<InvoiceHeader> InvoiceHeader { get; set; }
        public DbSet<InvoiceLineItem> InvoiceLineItem { get; set; }
        public DbSet<SalesSegment> SalesSegment { get; set; }
        public DbSet<SalesTerritory> SalesTerritory { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB; Initial Catalog=SPM_BI_Data");
                //.LogTo(Console.WriteLine, new[] { DbLoggerCategory.Database.Command.Name }, LogLevel.Information)
                //.EnableSensitiveDataLogging();
        }

        public override int SaveChanges()
        {
            this.ChangeTracker.DetectChanges();
            var added = this.ChangeTracker.Entries()
                        .Where(t => t.State == EntityState.Added)
                        .Select(t => t.Entity)
                        .ToArray();

            foreach (var entity in added)
            {
                if (entity is ITrackable)
                {
                    var track = entity as ITrackable;
                    track.CreatedDate = DateTime.Now;
                    track.CreatedBy = UserId;
                }
            }

            var modified = this.ChangeTracker.Entries()
                        .Where(t => t.State == EntityState.Modified)
                        .Select(t => t.Entity)
                        .ToArray();

            foreach (var entity in modified)
            {
                if (entity is ITrackable)
                {
                    var track = entity as ITrackable;
                    track.UpdatedDate = DateTime.Now;
                    track.UpdatedBy = UserId;
                }
            }
            return base.SaveChanges();

        }
    }
}
