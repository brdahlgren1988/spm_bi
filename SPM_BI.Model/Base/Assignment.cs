﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPM_BI.Model.Base
{
    public class Assignment : BaseModel
    {
        public int AssignmentID { get; set; }
        public SalesTerritory SalesTerritory { get; set; }
        public Employee Employee { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
}
