﻿using SPM_BI.Model.Interfaces;
using System;


namespace SPM_BI.Model.Base
{
    public abstract class BaseModel : ITrackable
    {
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
