﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPM_BI.Model.Base
{
    public class Customer : BaseModel
    {
        public int CustomerID { get; set; }
    }
}
