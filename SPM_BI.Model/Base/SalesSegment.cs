﻿
namespace SPM_BI.Model.Base
{
    public class SalesSegment : BaseModel
    {
        public int SalesSegmentID { get; set; }
        public string Description { get; set; }
    }
}
