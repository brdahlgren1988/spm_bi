﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPM_BI.Model.Base
{
    public class InvoiceHeader : BaseModel
    {
        public int InvoiceHeaderID { get; set; }

        public Customer Customer { get; set; }
        public int CustomerID { get; set; }
        public List<InvoiceLineItem> InvoiceLineItems { get; set; }

    }
}
