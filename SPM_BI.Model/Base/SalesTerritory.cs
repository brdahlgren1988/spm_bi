﻿
namespace SPM_BI.Model.Base
{
    public class SalesTerritory : BaseModel
    {
        public int SalesTerritoryID { get; set; }
        
        public int Division { get; set; }
        public int Group { get; set; }
        public int Organization { get; set; }
        public int Region { get; set; }
        public int Area { get; set; }
        public int Distric { get; set; }
        public int Territory { get; set; }
        public SalesSegment SalesSegment { get; set; }
        public int SalesSegmentID { get; set; }
    }
}
