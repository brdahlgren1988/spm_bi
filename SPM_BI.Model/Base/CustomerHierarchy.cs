﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPM_BI.Model.Base
{
    public class CustomerHierarchy
    {
        public int CustomerHierarchyID { get; set; }
        public string GlobalID { get; set; }
        public string CountryID { get; set; }
        public string FacilityID { get; set; }
        public Customer Customer { get; set; }
        public int CustomerID { get; set; }
    }
}
