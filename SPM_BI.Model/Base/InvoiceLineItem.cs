﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPM_BI.Model.Base
{
    public class InvoiceLineItem : BaseModel
    {
        public int InvoiceLineItemID { get; set; }
        public float Revenue { get; set; }
        public ChargeCode ChargeCode { get; set; }
        public int ChargeCodeID { get; set; }
        public InvoiceHeader InvoiceHeader { get; set; }
        public int InvoiceHeaderID { get; set; }
    }
}
